import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SambalComponent } from './sambal.component';

describe('SambalComponent', () => {
  let component: SambalComponent;
  let fixture: ComponentFixture<SambalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SambalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SambalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
