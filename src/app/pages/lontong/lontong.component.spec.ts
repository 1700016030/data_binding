import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LontongComponent } from './lontong.component';

describe('LontongComponent', () => {
  let component: LontongComponent;
  let fixture: ComponentFixture<LontongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LontongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LontongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
