import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaiComponent } from './petai.component';

describe('PetaiComponent', () => {
  let component: PetaiComponent;
  let fixture: ComponentFixture<PetaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
