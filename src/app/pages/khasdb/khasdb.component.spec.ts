import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KhasdbComponent } from './khasdb.component';

describe('KhasdbComponent', () => {
  let component: KhasdbComponent;
  let fixture: ComponentFixture<KhasdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KhasdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KhasdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
