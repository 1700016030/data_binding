import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-khasdb',
  templateUrl: './khasdb.component.html',
  styleUrls: ['./khasdb.component.css']
})
export class KhasdbComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

 ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				name: 'Ketan Durian Khas Sumatera Barat',
  				keterangan:'Resep Makanan Khas'
  			},
  			{
  				name: 'Ayam Pop Khas Padang',
  				keterangan:'Resep Makanan Khas'
  			},
  			{
  				name: 'Soto Sokaraja Khas Banyumas',
  				keterangan:'Resep Makanan Khas'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
