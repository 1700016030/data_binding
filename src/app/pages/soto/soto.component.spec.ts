import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SotoComponent } from './soto.component';

describe('SotoComponent', () => {
  let component: SotoComponent;
  let fixture: ComponentFixture<SotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
