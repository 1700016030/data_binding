import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KirimResepComponent } from './kirim-resep.component';

describe('KirimResepComponent', () => {
  let component: KirimResepComponent;
  let fixture: ComponentFixture<KirimResepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KirimResepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KirimResepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
