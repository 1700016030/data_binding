import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SotoBComponent } from './soto-b.component';

describe('SotoBComponent', () => {
  let component: SotoBComponent;
  let fixture: ComponentFixture<SotoBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SotoBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SotoBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
