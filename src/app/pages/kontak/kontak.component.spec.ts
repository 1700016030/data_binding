import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KONTAKComponent } from './kontak.component';

describe('KONTAKComponent', () => {
  let component: KONTAKComponent;
  let fixture: ComponentFixture<KONTAKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KONTAKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KONTAKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
