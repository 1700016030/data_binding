import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KetanDurianComponent } from './ketan-durian.component';

describe('KetanDurianComponent', () => {
  let component: KetanDurianComponent;
  let fixture: ComponentFixture<KetanDurianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KetanDurianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KetanDurianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
