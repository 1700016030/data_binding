import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sambaldb',
  templateUrl: './sambaldb.component.html',
  styleUrls: ['./sambaldb.component.css']
})
export class SambaldbComponent implements OnInit {
	
 constructor(config: NgbModalConfig, private modalService: NgbModal) { }

 ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				name: 'Sambal Dabu - Dabu',
  				keterangan:'Resep Sambal'
  			},
  			{
  				name: 'Sambal Mangga Muda',
  				keterangan:'Resep Sambal'
  			},
  			{
  				name: 'Sambal Petai',
  				keterangan:'Resep Sambal'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}

