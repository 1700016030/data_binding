import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SambaldbComponent } from './sambaldb.component';

describe('SambaldbComponent', () => {
  let component: SambaldbComponent;
  let fixture: ComponentFixture<SambaldbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SambaldbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SambaldbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
