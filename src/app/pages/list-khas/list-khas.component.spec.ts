import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListKhasComponent } from './list-khas.component';

describe('ListKhasComponent', () => {
  let component: ListKhasComponent;
  let fixture: ComponentFixture<ListKhasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListKhasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListKhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
