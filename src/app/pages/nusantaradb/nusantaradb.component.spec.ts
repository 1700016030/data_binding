import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NusantaradbComponent } from './nusantaradb.component';

describe('NusantaradbComponent', () => {
  let component: NusantaradbComponent;
  let fixture: ComponentFixture<NusantaradbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NusantaradbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NusantaradbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
