import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nusantaradb',
  templateUrl: './nusantaradb.component.html',
  styleUrls: ['./nusantaradb.component.css']
})
export class NusantaradbComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

 ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				name: 'Cara Membuat Pisang Nugget',
  				keterangan:'Resep Makanan Nusantara'
  			},
  			{
  				name: 'Cara Membuat Lontong dengan Cetakan',
  				keterangan:'Resep Makanan Nusantara'
  			},
  			{
  				name: 'Cara Membuat Soto Betawi',
  				keterangan:'Resep Makanan Nusantara'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
