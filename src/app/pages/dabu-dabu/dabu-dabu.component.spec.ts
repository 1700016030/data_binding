import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DabuDabuComponent } from './dabu-dabu.component';

describe('DabuDabuComponent', () => {
  let component: DabuDabuComponent;
  let fixture: ComponentFixture<DabuDabuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DabuDabuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DabuDabuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
